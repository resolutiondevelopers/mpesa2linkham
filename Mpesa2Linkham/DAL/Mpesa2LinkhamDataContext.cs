﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Mpesa2Linkham.DAL
{
    internal class Mpesa2LinkhamDataContext: DbContext
    {
        public Mpesa2LinkhamDataContext(DbContextOptions<Mpesa2LinkhamDataContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Models.MpesaModel> MpesaModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.MpesaModel>().ToTable("c2b2_callbacks").HasKey(x => x._id);
            base.OnModelCreating(modelBuilder);
        }
    }
}
