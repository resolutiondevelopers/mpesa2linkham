﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mpesa2Linkham.Profiles
{
    internal class Mpesa2LinkhamProfile: Profile
    {

        public Mpesa2LinkhamProfile()
        {
            CreateMap<Models.MpesaModel, DTOs.MpesaDetails>()
                .ForMember(src => src.TransTime, opt => opt.MapFrom(new TransTimeResolver()));
        }
    }
}
