﻿using AutoMapper;
using Mpesa2Linkham.DTOs;
using Mpesa2Linkham.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Mpesa2Linkham.Profiles
{
    internal class TransTimeResolver : IValueResolver<Models.MpesaModel, DTOs.MpesaDetails, string>
    {
        public string Resolve(MpesaModel source, MpesaDetails destination, string destMember, ResolutionContext context)
        {
            string dateFormat = "yyyyMMddHHmmss";
            DateTime dateTime = DateTime.ParseExact(source.TransTime, dateFormat, CultureInfo.InvariantCulture);

            return dateTime.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
