using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pomelo.EntityFrameworkCore.MySql;
using Microsoft.EntityFrameworkCore;
using Mpesa2Linkham.Repositories;
using AutoMapper;

namespace Mpesa2Linkham
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();

                    services.AddDbContext<DAL.Mpesa2LinkhamDataContext>(opts =>
                    {
                        opts.UseMySql(hostContext.Configuration.GetSection("MPESA:db").Value, opt =>
                        {
                            opt.EnableRetryOnFailure();

                        });
                    }, ServiceLifetime.Singleton);


                    services.AddSingleton<IRepository, Repository>();
                    services.AddSingleton<IWrapper, RepositoryWrapper>();
                    services.AddAutoMapper(typeof(Program));
                    services.AddSingleton<Services.IMpesa2LinkhamService, Services.Mpesa2LinkhamService>();
                });
    }
}
