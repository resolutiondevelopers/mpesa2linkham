﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mpesa2Linkham.Repositories
{
    internal interface IRepository
    {
        Task<IEnumerable<Models.MpesaModel>> GetAllAsync(Expression<Func<Models.MpesaModel,bool>> expression);
        Task UpdateAsync(int id,Models.MpesaModel model);
    }
}
