﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mpesa2Linkham.Repositories
{
    internal interface IWrapper
    {
        IRepository Repo { get; }
        Task SaveAsync();
    }
}
