﻿using Microsoft.EntityFrameworkCore;
using Mpesa2Linkham.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mpesa2Linkham.Repositories
{
    internal class Repository : IRepository
    {
        private readonly DAL.Mpesa2LinkhamDataContext _DataContext;
        public Repository(DAL.Mpesa2LinkhamDataContext dataContext)
        {
            this._DataContext = dataContext;
        }
        public async Task<IEnumerable<MpesaModel>> GetAllAsync(Expression<Func<MpesaModel, bool>> expression)
        {
            return await this._DataContext.Set<MpesaModel>().Where(expression).ToListAsync();
        }

        public async Task UpdateAsync(int id, MpesaModel model)
        {
            if (id == 0 || model == null) return;

            MpesaModel mpesaModel = await this._DataContext.Set<MpesaModel>().FindAsync(id);

            if(mpesaModel != null)
            {
                this._DataContext.Entry<MpesaModel>(mpesaModel).CurrentValues.SetValues(model);
            }
        }
    }
}
