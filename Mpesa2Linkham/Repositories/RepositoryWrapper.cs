﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mpesa2Linkham.Repositories
{
    internal class RepositoryWrapper : IWrapper
    {
        private IRepository _repository;
        private readonly DAL.Mpesa2LinkhamDataContext _context;

        public IRepository Repo
        {
            get { 
                if( _repository == null ) 
                    this._repository = new Repository(_context);
                return _repository;
            }
        }

        public async Task SaveAsync()
        {
            await this.SaveAsync();
        }

        public RepositoryWrapper(DAL.Mpesa2LinkhamDataContext context)
        {
            this._context = context;
        }
    }
}
