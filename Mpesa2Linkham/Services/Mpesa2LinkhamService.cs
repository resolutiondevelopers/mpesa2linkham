﻿using Microsoft.Extensions.Logging;
using Mpesa2Linkham.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using LinkhamHealthWebService;
using AutoMapper;

namespace Mpesa2Linkham.Services
{
    internal class Mpesa2LinkhamService : IMpesa2LinkhamService
    {
        private LinkhamHealthWebServiceSoapClient _soapClient;
        private DateTime InitDate = new DateTime(2022, 01, 25); // min date
        private readonly ILogger<Mpesa2LinkhamService> _logger;
        private readonly IWrapper _wrapper;
        private readonly IMapper _mapper;

        public Mpesa2LinkhamService(ILogger<Mpesa2LinkhamService> logger, IWrapper wrapper, IMapper mapper)
        {
            this._logger = logger;
            this._wrapper = wrapper;
            this._mapper = mapper;
            _soapClient = new LinkhamHealthWebServiceSoapClient(LinkhamHealthWebServiceSoapClient.EndpointConfiguration.LinkhamHealthWebServiceSoap12);
            
        }

        public async Task PostMpesa2Linkham()
        {

            try
            {

                IEnumerable<Models.MpesaModel> mpesaModels = await this._wrapper.Repo.GetAllAsync(p =>p.TrackStatus == 0 && p.Timestamp > InitDate);

                IEnumerable<DTOs.MpesaDetails> mpesaDetails = this._mapper.Map<IEnumerable<DTOs.MpesaDetails>>(mpesaModels);

                // serialize
                string json = JsonSerializer.Serialize<IEnumerable<DTOs.MpesaDetails>>(mpesaDetails);

                string jsonPayload = "{" + $"\"mpesaDetails\":{json}" + "}";

                string response = await _soapClient.MPesaTransactionDetailsPushAsync(jsonPayload);

                if (_logger.IsEnabled(LogLevel.Information))
                    _logger.LogInformation(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }

        }
    }
}
